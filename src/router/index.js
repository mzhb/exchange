import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home'
import Login from "../views/Login";
import Register from "../views/Register";
Vue.use(VueRouter)

const routes = [
  {
    name : "Home" ,
    path : '/' ,
    component : Home
  },
  {
    name : "Login" ,
    path : '/login' ,
    meta:{
      title : "ورود به حساب کاربری"
    } ,
    component: Login
  },
  {
    name: "Register" ,
    path: '/register' ,
    meta: {
      title: 'ثبت نام'
    } ,
    component: Register
  }
]

const router = new VueRouter({
  routes
})

export default router

const DEFAULT_TITLE = 'صرافی آنلاین';
router.afterEach((to, from) => {
  Vue.nextTick(() => {
    document.title = to.meta.title || DEFAULT_TITLE;
  });
});